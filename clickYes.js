window.onload = function () {
    setTimeout(clickYes, 1000);
}

function getModalHeadlineDiv() {
    return document.querySelector("div[role=heading]");
}

function clickYes() {
        const modalHeadlineDiv = getModalHeadlineDiv();
        if ((modalHeadlineDiv) !== undefined) {
            if (modalHeadlineDiv.textContent === 'Stay signed in?') {
                console.log("'Going to click Yes automatically'");
                const yesButton = document.querySelector("input[value=Yes]");
                if (yesButton) {
                    yesButton.click();
                } else {
                    console.log("No button found");
                }
            } else {
                console.log('No Headline Div with text \'Stay signed in?\ found.');
            }
        } else {
            console.log('No Headline Div found. Trying again in 1 second.');
            setTimeout(clickYes, 1000);
        }
}
