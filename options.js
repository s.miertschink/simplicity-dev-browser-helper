function savemrauthor() {
    const mrauthor = document.getElementById("mrauthor").value;

    chrome.storage.local.set({ simplicity_dev_helper_mrauthor: mrauthor }, function () {});
    document.getElementById("memrauthor").innerHTML = "saved.";
}

function saveatlassianUsername() {
    const atlassianUsername = document.getElementById("atlassianUsername").value;

    chrome.storage.local.set({ simplicity_dev_helper_atlassianUsername: atlassianUsername }, function () {});
    document.getElementById("meatlassianUsername").innerHTML = "saved.";
}

function load() {
    chrome.storage.local.get("simplicity_dev_helper_mrauthor", function (items) {
        document.getElementById("mrauthor").value = items.simplicity_dev_helper_mrauthor;
    });
    chrome.storage.local.get("simplicity_dev_helper_atlassianUsername", function (items) {
        document.getElementById("atlassianUsername").value = items.simplicity_dev_helper_atlassianUsername;
    });
}

document.addEventListener("DOMContentLoaded", load);

document.getElementById("savemrauthor").addEventListener("click", savemrauthor);
document.getElementById("saveatlassianUsername").addEventListener("click", saveatlassianUsername);
