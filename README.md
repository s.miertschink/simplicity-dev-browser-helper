# Simplicity Dev Browser Helper
This is a browser extension for Chrome, Edge, Brave, and other Chromium-based browsers.


### How to use
enable "Developer Mode" in the Chrome Extensions page, 
then click "Load unpacked" and select the folder containing the extension files.

### Configuration
Open Details of the extension and click on "Extension options"
