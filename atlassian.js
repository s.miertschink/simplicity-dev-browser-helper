window.onload = function () {
    setTimeout(fillMailAndClickOk, 1000);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function fillMailAndClickOk() {
    if (window.location.href.includes("https://id.atlassian.com/login")) {
        chrome.storage.local.get("simplicity_dev_helper_atlassianUsername", function (items) {
            if (items.simplicity_dev_helper_atlassianUsername) {
                atlassianUsername = items.simplicity_dev_helper_atlassianUsername;
                const mailInput = document.querySelector('input[id="username"]');
                mailInput.value = atlassianUsername;
                let event = new Event('input', {bubbles: true});
                mailInput.dispatchEvent(event);
                const button = document.querySelector('button[id="login-submit"]');
                button.click();
            }
        });
    }
}
