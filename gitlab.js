window.onload = function () {
    setTimeout(markReviewedGreen, 1400);
}


async function markReviewedGreen() {
    let mrAuthor = "";
    chrome.storage.local.get("simplicity_dev_helper_mrauthor", function (items) {
        if (items.simplicity_dev_helper_mrauthor) {
            mrAuthor = items.simplicity_dev_helper_mrauthor;
        }

    });

    if (window.location.href.includes("https://gitlab.com")) {
        chrome.storage.local.get("simplicity_dev_helper_mrauthor", function (items) {
            if (items.simplicity_dev_helper_mrauthor) {

                // already approved by me
                // funktioniert grad nicht, weil Gitlab die UI angepasst hat und nicht mehr
                // zu sehen ist, ob man selbst schon approved hat
                const mrAuthor = items.simplicity_dev_helper_mrauthor;
                let xpathQuery = `//span[contains(@title, "you've approved")]`;
                let mrTable = document.getElementById("content-body").querySelector('ul.content-list');

                let xpathResult = document.evaluate(
                    xpathQuery,
                    mrTable,
                    null,
                    XPathResult.ORDERED_NODE_ITERATOR_TYPE,
                    null
                );
                let currentNode = xpathResult.iterateNext();
                const approvedByMe = [];
                while (currentNode) {
                    const node = currentNode.parentNode.parentNode.parentNode.parentNode.parentNode
                    node.style.backgroundColor = 'rgb(18 86 36)';
                    approvedByMe.push(node);
                    currentNode = xpathResult.iterateNext();
                }
                for (const node of approvedByMe) {
                    node.remove();
                    mrTable.appendChild(node);
                }

                // created by me
                mrTable = document.getElementById("content-body").querySelector('ul.content-list');
                xpathQuery = `//span[@class="author" and text()="${mrAuthor}"]`;
                xpathResult = document.evaluate(
                    xpathQuery,
                    mrTable,
                    null,
                    XPathResult.ORDERED_NODE_ITERATOR_TYPE,
                    null
                );

                currentNode = xpathResult.iterateNext();
                console.warn(currentNode);
                const createdByMe = [];
                while (currentNode) {
                    const node = currentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode
                    node.style.backgroundColor = 'rgb(14 42 111)';
                    createdByMe.push(node);
                    currentNode = xpathResult.iterateNext();
                }
                for (const node of createdByMe) {
                    node.remove();
                    mrTable.appendChild(node);
                }


                // created by Renovate Bot
                mrTable = document.getElementById("content-body").querySelector('ul.content-list');
                xpathQuery = `//span[@class="author" and text()="Simplicity Networks"]`;
                xpathResult = document.evaluate(
                    xpathQuery,
                    mrTable,
                    null,
                    XPathResult.ORDERED_NODE_ITERATOR_TYPE,
                    null
                );

                currentNode = xpathResult.iterateNext();
                const createdByBot = [];
                while (currentNode) {
                    const node = currentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode
                    node.style.backgroundColor = 'rgb(86 71 67)';
                    createdByBot.push(node);
                    currentNode = xpathResult.iterateNext();
                }
                for (const node of createdByBot) {
                    node.remove();
                    mrTable.appendChild(node);
                }
            }
        });
    }
}
